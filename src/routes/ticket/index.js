const express = require("express");
const ticketController = require("./ticket");
const { authenticate, authorize } = require("../../middleware/auth");


const router = express.Router();

// router.get("/", stationController.getStations)
// router.get("/:id", stationController.getStationById)
// // router.post("/", authenticate, authorize["amdin"], stationController.createStation)
// router.put("/:id", stationController.replaceStationById)
// router.patch("/:id", stationController.updateStationById)
// router.delete("/:id", stationController.deleteStationById)

router.post("/", authenticate, authorize(["client"]), ticketController.createTicket)

module.exports = router;