// const { Ticket } = require("../../models/Ticket");
// const { Trip } = require('../../models/Trip');

// const _ = require('lodash');

// module.exports.createTicket = async (req, res, next) => {
//     const { trip_id, seatCodes } = req.body;
//     try {

//         const trip = await Trip.findById(trip_id)
//         if (!trip) return res.status(500).json({ message: "TRIP DO NOT EXISTS" });

//         const availableSeatCodes = trip.seats
//             .filter(s => !s.isBooked)
//             .map(s => s.code);

//         const errSeatCodes = seatCodes.filter(s => {
//             return availableSeatCodes.indexOf(s) === -1
//         })
//         if (!errSeatCodes) return res.status(500).json({ message: "" })

//         if (_.isEmpty(errSeatCodes)) return json({ message: `Seat(s) ${errSeatCodes.join(",")} are already booked`, errSeatCodes })

//         const newTicket = new Ticket({
//             trip_id,
//             user_id = req.user_id,
//             seats: seatCodes.map(code => new Seat({ code, isBooked: true }))
//         })

//         seatCodes.forEach(code => {
//             const index = trip.seats.findIndex(s => s.code === code)
//             trip.seats[index].isBooked = true;
//         });

//         await newTicket.save()
//         await trip.save()
//     } catch (e) {
//         console.log(e);
//     }
// }

// const { Trip } = require("../../models/Trip")
// const { Ticket } = require("../../models/Ticket")
// const { Seat } = require("../../models/Seat")
// const { sendBookTicketEmail } = require("../../email/bookingTicket")
// const _ = require("lodash");

// module.exports.createTicket = (req, res, next) => {
//     const { tripId, seatCode } = req.body;
//     Trip.findById(tripId)
//         .then(trip => {
//             if (!trip) return Promise.reject({ message: "Trip not found" })

//             const availableSeatCode = trip.seat
//                 .filter(s => !s.isBooked)
//                 .map(s => s.code)

//             // const errorSeatCode = seatCode.map(s => {
//             //     if(availableSeatCode.indexOf(s) === -1 ) return s;
//             // })

//             const errorSeatCode = seatCode.filter(s => availableSeatCode.indexOf(s) === -1)

//             // const errorSeatCode = seatCode.filter(s => {
//             //     if(availableSeatCode.indexOf(s) === -1 ) return true;
//             // })

//             if (!_.isEmpty(errorSeatCode)) return Promise.reject({
//                 message: `Seat(s) ${errorSeatCode.join(",")} are already booked`,
//                 errorSeatCode
//             })

//             const newTicket = new Ticket({
//                 tripId,
//                 userId: req.user._id,
//                 seats: seatCode.map(code => new Seat({ code, isBooked: true })),
//                 totalPrice: seatCode.length * trip.price
//             })

//             seatCode.forEach(code => {
//                 const index = trip.seat.findIndex(s => s.code == code)
//                 trip.seat[index].isBooked = true
//             });

//             return Promise.all([
//                 newTicket.save(),
//                 trip.save()
//             ])

//         })
//         .then(result => {
//             sendBookTicketEmail()
//             result.status(200).json(result(0))
//         })
//         .catch(err => err.status(500).json(err))

// }
// module.exports.createTicket = (req, res, next) => {
//     const { tripId, seatCodes } = req.body;
//     // seatCodes = ['A01', 'A02', 'A03']
//     Trip.findById(tripId)
//         .then(trip => {
//             if (!trip) return Promise.reject({ message: "Trip not found" })

//             // ['A01', 'A04', 'A05',....]
//             const availableSeatCodes = trip.seats
//                 .filter(s => !s.isBooked)
//                 .map(s => s.code)

//             // ['A02', 'A03']
//             // const errSeatCodes = [];
//             // seatCodes.forEach(s => {
//             //   if (availableSeatCodes.indexOf(s) === -1) {
//             //     errSeatCodes.push(s)
//             //   }
//             // })

//             const errSeatCodes = seatCodes.filter(s => {
//                 // if (availableSeatCodes.indexOf(s) === -1) return true
//                 return availableSeatCodes.indexOf(s) === -1
//             })

//             // const errSeatCodes = seatCodes.map(s => {
//             //   if (availableSeatCodes.indexOf(s) === -1) return s
//             // })

//             if (!_.isEmpty(errSeatCodes)) return Promise.reject({
//                 message: `Seat(s) ${errSeatCodes.join(", ")} are already booked`,
//                 errSeatCodes
//             })

//             const newTicket = new Ticket({
//                 tripId,
//                 userId: req.user._id,
//                 seats: seatCodes.map(code => new Seat({ code, isBooked: true })),
//                 totalPrice: seatCodes.length * trip.price
//             })

//             seatCodes.forEach(code => {
//                 const index = trip.seats.findIndex(s => s.code === code)
//                 trip.seats[index].isBooked = true;
//             })

//             return Promise.all([
//                 newTicket.save(),
//                 trip.save()
//             ])
//         })
//         .then(result => {
//             sendBookTicketEmail();
//             res.status(200).json(result[0])
//         })
//         .catch(err => res.json(err))
// }

const { Ticket } = require("../../models/Ticket")
const { Seat } = require("../../models/Seat")
const { Trip } = require("../../models/Trip")

module.exports.createTicket = async (req, res, next) => {
    const { fare, user_id, seat_code, car_id, station_from_id, station_to_id } = req.body;
    try {

        const car = await Seat.findById(car_id);
        if (car) return res.status(500).json({ message: "CAR DO NOT EXISTS" });

        // const availableSeatCodes = car.isBooked;
        // if (availableSeatCodes === true) return res.status(500).json({ message: "SEAT ALREADY BOOKED" })

        const newTicket = new Ticket({
            fare, user_id, seat_code, car_id, station_from_id, station_to_id
        })
        availableSeatCodes = true;
        newTicket.save();
        return res.status(200).json({ message: "BOOK_SUCCESSFUL" })
    } catch (e) {
        console.log(e);
    }
}