const express = require("express");
const seatController = require("./seat");
const { authenticate, authorize } = require('../../middleware/auth');


const router = express.Router();

// router.get("/", stationController.getStations)
// router.get("/:id", stationController.getStationById)
// // router.post("/", authenticate, authorize["amdin"], stationController.createStation)
// router.put("/:id", stationController.replaceStationById)
// router.patch("/:id", stationController.updateStationById)
// router.delete("/:id", stationController.deleteStationById)
router.get("/:id", seatController.getSeatById)
router.post("/", authenticate, authorize(["admin"]), seatController.createSeat);

module.exports = router;