
const { Seat } = require("../../models/Seat");

// module.exports.getAllSeat = async (req, res, next) => {
//     const { id } = req.params;
//     try {
//         const seat = await Seat.find(id);
//         if (seat) return res.status(500).json({ message: "SEAT DO NOT EXISTS" })
//         return res.status(200).json({ message: "GET SUCCESSFUL" })
//     } catch (e) {
//         console.log(e);
//     }
// }

// module.exports.getSeatById = async (req, res, next) => {
//     try {

//     } catch (e) {

//     }
// }
module.exports.getSeatById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const seat = await Seat.findById(id)
            .populate("car_id", "name")
        if (!seat) return res.status(500).json({ message: "SEAT DO NOT EXISTS" });
        return res.status(200).json({ message: "GET_SUCCESSFUL", seat })
    } catch (e) {
        console.log(e);
    }
}

module.exports.createSeat = async (req, res, next) => {
    const { code, car_id } = req.body;
    try {
        const seat = await Seat.findOne({ code: code, car_id: car_id });
        if (seat) return res.status(500).json({ message: "SEAT ALREADY EXISTS" })
        const newSeat = new Seat({
            code, car_id
        })
        newSeat.save();
        return res.status(200).json({ message: "CREATE SUCCESSFUL", newSeat })
    } catch (e) {
        console.log(e);
    }
}
