const express = require('express');
const router = express.Router();


//Station
router.use("/stations", require("./station"))
//Trip
router.use("/trips", require("./trip"))
//Car
router.use("/cars", require("./car"))
//Seat
router.use("/seats", require("./seat"))
//Ticket
router.use("/tickets", require("./ticket"))
//User
router.use("/user", require("./user"))



module.exports = router;