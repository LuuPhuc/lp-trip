const express = require("express");
const carController = require("./car");
const { authenticate, authorize } = require('../../middleware/auth');


const router = express.Router();

// router.get("/", stationController.getStations)
// router.get("/:id", stationController.getStationById)
// // router.post("/", authenticate, authorize["amdin"], stationController.createStation)
// router.put("/:id", stationController.replaceStationById)
// router.patch("/:id", stationController.updateStationById)
// router.delete("/:id", stationController.deleteStationById)

router.get("/", carController.getAllCar);
router.get("/:id", carController.getCarById);
router.post("/", authenticate, authorize(["admin"]), carController.createCar);


module.exports = router;