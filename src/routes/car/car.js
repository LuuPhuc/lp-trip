
const { Car } = require("../../models/Car");

const { Send } = require("../../common/Send");

module.exports.getAllCar = async (req, res, next) => {
    try {
        const car = await Car.find()
        if (!car) return res.status(500).json({ message: "CAR DO NOT EXISTS" })
        return res.status(200).json(car)
    } catch (e) {
        console.log(e);
    }
}

module.exports.getCarById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const car = await Car.findById(id)
        if (!car) return res.status(500).json({ message: "CAR DO NOT EXISTS" })
        return Send.success;
    } catch (e) {
        console.log(e);
    }
}

module.exports.createCar = async (req, res, next) => {
    const { name, brand_name, lincese_plates } = req.body;
    try {
        const car = await Car.findOne({ lincese_plates: lincese_plates })
        if (car) return res.status(500).json({ message: "CAR ALREADY EXISTS" })

        const newCar = new Car({
            name, brand_name, lincese_plates
        })
        newCar.save();
        return res.status(200).json({message: "CREATE SUCCESSFUL", newCar})
        // return Send.success(res, newCar);
    } catch (e) {
        console.log(e);
    }
}


