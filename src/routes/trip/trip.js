// const { Trip } = require('../../models/Trip');
// const { send } = require('../../common/Send')

// module.exports.getTrip = async (req, res, next) => {
//     try {
//         const trips = await Trip.find()
//         .populate("car_id")
//         .populate("station_form_id")
//         .select("seats")
//         if (!trips) return console.log("TRIP NOT FOUND");
//         return send.success(res, "GET_SUCCESS")
//     } catch (e) {
//         console.log(e);
//         return send.fail(res, "SOME_THING_WRONG")
//     }
// }

// module.exports.createTripById = async (req, res, next) => {
//     const { name, car_id, station_form_id, station_to_id, departure_time, arrival_time } = req.body;
//     try {
//         const newTrips = new Trip({
//             name, car_id, station_form_id, station_to_id, departure_time, arrival_time
//         })
//         newTrips.save();
//         return res.status(200).json(newTrips)
//     } catch (e) {
//         console.log(e);
//         return send.fail(res, "SOME_THING_WRONG")
//     }

// }


// module.exports.replaceTripById = async (req, res, next) => {

// }


// module.exports.updateTripById = async (req, res, next) => {

// }

const { Trip } = require("../../models/Trip");

// const { Seat } = require("../../models/Seat");
// const seatCodes = [
//     "A01", "A02", "A03", "A04", "A05", "A06", "A07", "A08", "A09", "A010", "A11", "A12",
//     "B01", "B02", "B03", "B04", "B05", "B06", "B07", "B08", "B09", "B010", "B11", "B12"
// ]

// module.exports.getTrips = (req, res, next) => {
//     Trip.find()
//         .populate("fromStationId")
//         .populate("toStationId")
//         .select("-seats")
//         .then(trips => res.status(200).json(trips))
//         .catch(err => res.status(500).json(err))
// }

// module.exports.getTripById = (req, res, next) => {

// }

// module.exports.createTrip = (req, res, next) => {
//     const {fromStationId, toStationId, startTime, price, seats } = req.body;
//     // const seats = [];
//     // seatCodes.forEach(code => {
//     //   const newSeat = new Seat({ code: code })
//     //   seats.push(newSeat)
//     // })
//     const seats = seatCodes.map(code => new Seat({ code }))

//     const newTrip = new Trip({
//         fromStationId, toStationId, startTime, price, seats
//     })
//     newTrip.save()
//         .then(trip => res.status(201).json(trip))
//         .catch(err => res.status(500).json(err))
// }
module.exports.getTripById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const trip = await Trip.findById(id)
            .populate("car_id", "name")
            .populate("station_from_id", "name")
            .populate("station_to_id", "name")
        if (!trip) return res.status(500).json({ message: "TRIP DO NOT EXISTS" })
        return res.status(200).json({ message: "GET_SUCCESSFUL" , trip})
    } catch (e) {
        console.log(e);
    }
}

module.exports.createTrip = async (req, res, next) => {
    const { name, car_id, station_from_id, station_to_id } = req.body;
    try {
        // const stationFrom = await Trip.findOne(station_from_id);
        // const stationTo = await Trip.findOne(station_to_id);
        // if (stationFrom === stationTo)
        //     return res.status(500).json({ message: "STATION FROM CAN'T COINCIDE STAION TO" });
        const trip = new Trip({
            name, car_id, station_from_id, station_to_id
        })
        trip.save();
        console.log(trip);
        return res.status(200).json({ message: "CREATE SUCCESSFUL", trip })
    } catch (e) {
        console.log(e);
    }
}
module.exports.replaceTripById = (req, res, next) => {

}

module.exports.updateTripById = (req, res, next) => {

}

module.exports.deleteById = (req, res, next) => {

}