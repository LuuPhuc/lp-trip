const express = require("express");
const tripController = require("./trip");
const { authenticate, authorize } = require('../../middleware/auth');


const router = express.Router();

router.get("/:id", tripController.getTripById)
router.post("/", authenticate, authorize(["admin"]), tripController.createTrip);

// router.get("/:id", tripController)
// router.post("/", stationController.createStation)
// router.put("/:id", stationController.replaceStationById)
// router.patch("/:id", stationController.updateStationById)
// router.delete("/:id", stationController.deleteStationById)

module.exports = router;