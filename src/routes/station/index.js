const express = require("express");
const stationController = require("./station");
const { authenticate, authorize } = require('../../middleware/auth');


const router = express.Router();

router.get("/", stationController.getStations)
router.get("/:id", stationController.getStationById)

router.post("/", authenticate, authorize(["admin"]), stationController.createStation)

router.put("/:id", stationController.replaceStationById)
router.patch("/:id", stationController.updateStationById)
router.delete("/:id", stationController.deleteStationById)

module.exports = router;