const express = require('express');
const router = express.Router();

const { Station } = require('../../models/Station');

// router.get('/stations', (req, res, next) => {
//     Station.find()
//         .then(stations => res.status(200).json(stations))
//         .catch(err => console.log(err));
// })

// router.post('/stations', (req, res, next) => {
//     const { name, address, province, phone } = req.body;
//     const newStation = new Station({
//         name, address, province, phone
//     })
//     newStation.save()
//         .then(station => res.status(200).json(station))
//         .catch(err => res.status(500).json(err))
// })

// // router.get('/', (req, res, next) => {
// //     console.log("MDW1");
// //     next()
// // }, (req, res, next) => { 
// //     console.log("MDW2");
// // })
// //Middle Ware bản chất là những callback function

// router.put('/stations/:id', (req, res, next) => {
//     const { id } = req.params;
//     const { name, address, province } = req.body;
//     Station.findById(id)
//         .then(station => {
//             if (!station) return console.log("STATION DO NOT EXIST")
//             station.name = name;
//             station.address = address;
//             station.province = province;
//             return station.save()
//         })
//         .catch(err => res.status(500).json(err))
// })

// router.put('/stations/:id', async (req, res, next) => {
//     const { id } = req.params;
//     const { name, address, province } = req.body;
//     try {
//         const station = Station.findOne(id);
//         if (!station) return console.log("STATION DO NOT EXIST")
//         await station.update(data => {
//             data.name = name;
//             data.address = address;
//             data.province = province
//         });
//         station.save();
//     } catch (err) {
//         console.log(err);
//     }
// })

// router.delete('/station/:id', async (req, res, next) => {
//     const { id } = req.params;
//     try {
//         const station = await Station.findById(id)
//         if (!station) return console.log("STATION DO NOT EXIST");
//         station.remove();
//         return console.log("DELETE SUCCESS FUl");
//     } catch (err) {
//         console.log(err);
//     }

// })


// module.exports = router;

module.exports.getStations = (req, res, next) => { // req: request, res: response, next
  Station.find()
    .then(stations => res.status(200).json(stations))
    .catch(err => res.status(500).json(err))
}

module.exports.getStationById = (req, res, next) => {
  const { id } = req.params;
  Station.findById(id)
    .then(station => {
      if (!station) return Promise.reject({ message: "Station not found" })

      res.status(200).json(station)
    })
    .catch(err => res.json(err))
}

module.exports.createStation = (req, res, next) => {
  const { name, address, phone, province } = req.body;
  Station.create({
    name, address, phone, province
  })
    .then(station => res.status(201).json(station))
    .catch(err => res.status(500).json(err))
}

//PUT
module.exports.replaceStationById = (req, res, next) => {
  const { id } = req.params;
  // const { name, address, province } = req.body;
  Station.findById(id)
    .then(station => {
      if (!station) return Promise.reject({ message: "Station not found" })

      // station.name = name
      // station.address = address
      // station.province = province
      return station.save()
    })
    .then(station => res.status(200).json(station))
    .catch(err => res.json(err))
}

//PACTH
module.exports.updateStationById = async (req, res, next) => {
  const { id } = req.params;
  // const { name, address, province } = req.body;
  try {
    const station = await Station.findById(id)
    if (!station) return console.log({ message: "Station not found" });

    // if (name) station.name = name;
    // if (address) station.address = address;
    // if (province) station.province = province;
    Object.keys(req.body).forEach(key => station[key] = req.body[key]);
    station.save();
    return res.status(200).json({message: "UPDATE SUCCESSFUL"})
  } catch (e) {
    console.log(e);
  }
}

module.exports.deleteStationById = (req, res, next) => {
  const { id } = req.params;
  Station.deleteOne({ _id: id })
    .then(() => res.status(200).json({ message: "Delete successfully" }))
    .catch(err => res.json(err))
}
