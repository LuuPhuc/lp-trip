const { promisify } = require('util');

const jwt = require('jsonwebtoken');
const { User } = require('../../models/User');
const { send } = require('../../common/Send');
const { cryptPassword, comparePassword } = require('../../middleware/password')

const _ = require('lodash')

const bcrypt = require('bcryptjs');
const jwtSign = promisify(jwt.sign);

//Promise
module.exports.registerUser = (req, res, next) => {
    const { userType, name, email, password, phone, gender, dateOfBirth } = req.body;
        const newUser = new User({ userType, name, email, password, phone, gender, dateOfBirth })
        // bcrypt.genSalt(10, (err, salt) => {
        //     bcrypt.hash(password, salt, (err, hash) => {
        //         newUser.password = hash;
        //     })
        // })
        // newUser.save()
        //     .then(user => res.status(200).json(user))
        //     .catch(err => res.status(500).json(err))
        //Callback
        console.log(newUser)
        newUser.save()
        .then(user => res.status(200).json(user))
        .catch(err => res.status(500).json(err))
}
// module.exports.registerUser = async (req, res, next) => {
//     const { userType, name, email, password, phone, gender, dateOfBirth } = req.body;
//     try {
//         const user = await User.findOne({ email })
//         if (user) return res.status(500).json({ message: "USER ALREADY EXISTS" })
//         const passwordHash = await cryptPassword(password, 10, );
//         const newUser = new User({ userType, name, password: passwordHash, phone, gender, dateOfBirth })
//         return newUser.save();
//     } catch (e) {
//         console.log(e);
//     }
// }

module.exports.updateEmail = async (req, res, next) => {

}

module.exports.updatePassword = async (req, res, next) => {

}

//send newPassword to email
module.exports.resetPassword = async (req, res, next) => {

}

module.exports.Login = async (req, res, next) => {
    const { email, password } = req.body;
    try {
        const user = await User.findOne({ email })
        if (!email) Promise.reject({ message: "EMAIL DO NOT EXISTS" })
        const comparePassword = await bcrypt.compare(password, user.password);

        if (!comparePassword) return res.json({ message: "WRONG PASSWORD" })
        const payload = _.pick(user, ["email", "name", "userType"])

        const token = await jwtSign(
            payload,
            "LuuPhuc",
            { expiresIn: "1h" }
        )
        return res.status(200).json({ message: "LOGIN SUCCESSFUL", token })

    } catch (e) {
        console.log(e);
    }
}

module.exports.uploadAvatar = async (req, res, next) => {
    const { _id } = req.user;
    try {
        const user = User.findById(id)
        if (!user) return res.json({ message: "USER DO NOT EXISTS" })
        user.avatar = req.file.path;
        return user.save();
    } catch (e) {
        console.log(e);
    }
}