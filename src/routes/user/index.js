const express = require("express");
const userController = require("./user");
const { authenticate } = require('../../middleware/auth');
const { uploadImage } = require('../../middleware/uploadImage')
const { validatateRegisterUser } = require('../../middleware/validatation/user');

const router = express.Router();

router.post('/register', validatateRegisterUser, userController.registerUser);
router.post('/login', userController.Login);

router.post('/upload',
    authenticate,
    uploadImage("avatar"),
    userController.uploadAvatar
);

module.exports = router;