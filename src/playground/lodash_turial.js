const _ = require('lodash');

const person1 = {
    name: "Nguyen Van A",
    job: {
        title: "student",
        university: {
            name: "UIT",
            major: "MMT"
        }
    }
}

const person2 = {
    name: "Le Quang Song",
    job: {
        title: "bussiness",
        offices: 
        [
            {
                name: "VP Q10",
                address: "Q10"
            },
            {
                name: "VP Q10",
                address: "Q10"
            }
        ]
    }
}

//1._Get
const person = [person1, person2]
person.forEach(p => {
    console.log(_.get(p, "job.university.major", "Khong co major"));
})

//2._set
_.set(person2, "job.univerisity.major" , "AI")
console.log(person2);