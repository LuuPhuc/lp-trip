const validator = require('validator');
const _ = require("lodash");

const { User } = require('../../models/User')

module.exports.validatateRegisterUser = async (req, res, next) => {
    let errors = {};

    //validate
    const name = _.get(req, "body.name", "");
    const email = _.get(req, "body.email", "");
    const password = _.get(req, "body.password", "");

    if (validator.isEmpty(email)) { //Check empty, null, 
        errors.email = "Email is require";
    } else {
        const user = await User.findOne({ email })
        if (user) {
            errors.email = "Email exists";
        } else if (!validator.isEmail(email)) {
            errors.email = "Email is valid"
        }
    }

    if (validator.isEmpty(password)) {
        errors.password = "Password is required"
    } else if (!validator.isLength(password, { min: 8 })) {
        errors.password = "Password have to 8 at least characters"
    }

    if (validator.isEmpty(name)) {
        errors.name = "Name is required"
    }

    const isValid = _.isEmpty(errors)
    if (isValid) return next();
    return res.status(400).json(errors)

    // return {
    //     isValid: _.isEmpty(errors), //Check Array, Object
    //     errors
    // }
}