const { jwtVerify } = require('../helper/callback_helper');

// module.exports.authenticate = async (req, res, next) => {
//     try {
//         const token = req.header("token");
//         if (!token) return res.status(200).json({ message: "MUST BE PROVICE TOKEN" })
    
//         const verify = jwtVerify(token, "LuuPhuc")
//         if (!verify) return res.status(200).json({ message: "TOKEN VALID" })
//         req.user = verify
//         return next();
//     } catch (e) {
//         console.log(e);
//     }
// }
module.exports.authenticate = (req, res, next) => {
    const token = req.header("token");
    if(!token) return res.status(401).json({message: "YOU MUST BE PROVICE TOKEN"})
    jwtVerify(token, "LuuPhuc")
    .then(decoded => {
        if(decoded) {
            req.user = decoded;
            return next();
        }
        return res.status(200).json({message: "TOKEN INVALID"})
    })
    .catch(err => res.status(500).json(err))
}
module.exports.authorize = (userTypeArray) => (req, res, next) => {

    const { userType } = req.user;
    const index = userTypeArray.findIndex(e => e === userType)
    if (index > -1) return next()

    res.status(403).json({ message: "You are not allow to access" })
}
