const Send = {
    success: (res, data) => {
        res.status(201).json({ message: "SUCCESSFUL", data })
    },
    fail: (res) => {
        res.status(422).json({ message: "FAIL"});
    },
    error: (res, error) => {
        res.status(500).json({ message: "SOME_THING_WRONG", error});
    }
}
module.exports = Send;