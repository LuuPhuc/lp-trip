const mongoose = require('mongoose');
const validator = require('validator');
const { promisify } = require('util');
const bcrypt = require('bcryptjs');

//Bcrypt
const genSaltPromise = promisify(bcrypt.genSalt);
const hashPromise = promisify(bcrypt.hash);

const UserSchema = new mongoose.Schema({
    userType:
    {
        type: String,
        default: "client"
    },
    name:
    {
        type: String,
        require: true
    },
    avatar: 
    {
        type: String,
        default: null
    },
    email:
    {
        type: String, trim: true, require: true,
        unique: true,
        require: true,
    },
    password:
    {
        type: String,
        require: true
    },
    phone:
    {
        type: String,
        unique: true,
        require: true,
    },
    gender: 
    {
        type: Number,
        require: true,
        default: 1
    },
    dateOfBirth:
    {
        type: Date,
        require: true
    }
}, {timestamps: true});

UserSchema.pre("save", function (next) {
    const user = this;
    if (!user.isModified("password")) return next();
    
    genSaltPromise(10)
        .then(salt => hashPromise(user.password, salt))
        .then(hash => {
            user.password = hash;
            next();
        })
        .catch(err => {
        throw Error(err)
    })
})

const User = mongoose.model("User", UserSchema, "User")

module.exports = {
    UserSchema, User
}