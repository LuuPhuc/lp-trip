const mongoose = require("mongoose");

const CarSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    brand_name: {
        type: String,
        require: true
    },
    lincese_plates: {
        type: String,
        require: true
    }
}, {timestamps: true})

const Car = mongoose.model("Car", CarSchema, "Car");

module.exports = {
    CarSchema, Car
}