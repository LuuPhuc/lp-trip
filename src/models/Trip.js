// const mongoose = require("mongoose");
// const { SeatSchema } = require("./Seat");

// const TripSchema = new mongoose.Schema({
//   fromStationId: {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: "Station"
//   },
//   toStationId: {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: "Station"
//   },
//   startTime: {
//     type: Date,
//     required: true
//   },
//   price: {
//     type: Number,
//     required: true
//   },
//   seats: [SeatSchema]
// })

// const Trip = mongoose.model("Trip", TripSchema, "Trip")

// module.exports = {
//   TripSchema, Trip
// }

const mongoose = require("mongoose");

const TripSchema = new mongoose.Schema({
    name: {
      type: String,
      require: true
    },
    car_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Car"
    },
    station_from_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Station"
    },
    station_to_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Station"
    },
    // depature_time: {
    //   type: Date,
    //   require: true
    // },
    // arrival_time: {
    //   type: Date,
    //   require: true
    // }
}, {timestamps: true});

const Trip = mongoose.model("Trip", TripSchema, "Trip")

module.exports = {
  TripSchema, Trip
}
