const mongoose = require("mongoose");

const TicketSchema = new mongoose.Schema({
    seat_code: {
        type: String,
        require: true
    },
    fare: {
        type: String,
        require: true,
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    car_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Car"
    },
    station_from_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Station"
    },
    station_to_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Station"
    },
    totalPrice: { 
        type: Number, 
        required: true 
    }
}, { timestamps: true })

const Ticket = mongoose.model("Ticket", TicketSchema, "Ticket");

module.exports = {
    TicketSchema, Ticket
}