const mongoose = require("mongoose");

const SeatSchema = new mongoose.Schema({
  code: {
    type: String,
    required: true
  },
  isBooked: {
    type: Boolean,
    default: false
  },
  car_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Car"
  }
}, { timestamps: true })

const Seat = mongoose.model("Seat", SeatSchema, "Seat")

module.exports = {
  SeatSchema, Seat
}