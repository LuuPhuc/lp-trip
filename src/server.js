const express = require('express');
const mongoose = require('mongoose');

//Server config
const srvConfig = require('./config/config');

//Create global
const app = express();

//Middle ware

//Models
require('./models/Station');
// require('./models/Parent');
// require('./models/Student');

// Use static files
app.use(express.static('public'))
app.use(express.json());

app.use("/images", express.static("uploads"))


app.use("/api", require("./routes"));

//Connect to DB
const Data = require('./config/mongodb');
mongoose.connect(Data.MongoURI, { useNewUrlParser: true, useUnifiedTopology: true }, () => {
    console.log("Connected DB"); 
});

app.listen(srvConfig.PORT, () => {
    console.log("Server started on port 8080")
})


